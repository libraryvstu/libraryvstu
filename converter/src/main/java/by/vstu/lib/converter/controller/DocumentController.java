package by.vstu.lib.converter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import by.vstu.lib.converter.entity.DocumentWrapper;
import by.vstu.lib.converter.entity.marc.Document;
import by.vstu.lib.converter.service.dspace.MetaService;
import by.vstu.lib.converter.service.marc.DocumentService;
import by.vstu.lib.converter.util.exporter.DocumentExporter;

@PreAuthorize("hasRole('ADMIN')")
@Controller
@RequestMapping("/")
public class DocumentController {

	@Autowired
	private DocumentService documentService;
	@Autowired
	private MetaService metaService;
	@Autowired
	private DocumentExporter documentExporter;

	@GetMapping
	public String index(@RequestParam(required = false) Integer pageNumber, ModelMap map) {
		if (pageNumber == null) {
			pageNumber = 0;
		}
		PageRequest request = new PageRequest(pageNumber, 50);
		Page<Document> page = documentService.readWithSourcesAndHeaders(request);
		List<Document> documents = page.getContent();
		map.addAttribute("documents", documents);
		map.addAttribute("pages", page.getTotalPages());
		return "index";
	}

	@GetMapping("documents/{id}")
	public String documents(@PathVariable Integer id, ModelMap map) {
		try {
			Document document = documentService.readEager(id);
			System.out.println(document.getUrls());
			DocumentWrapper documentWrapper = new DocumentWrapper(document);
			map.addAttribute("document", documentWrapper);
			map.addAttribute("collections", metaService.readCollections());
			return "documents";
		} catch (Exception e) {
			map.addAttribute("cause", "Невозможно прочитать документ");
			map.addAttribute("exception", e);
			return "error";
		}
	}

	@GetMapping("/search")
	public String getResults(@RequestParam final String query, @RequestParam(required = false) Integer pageNumber,
			final ModelMap map) {
		if (pageNumber == null) {
			pageNumber = 0;
		}
		PageRequest request = new PageRequest(pageNumber, 50);
		Page<Document> page = documentService.findBySourceNameContainingWithSources(query, request);
		List<Document> documents = page.getContent();
		map.addAttribute("documents", documents);
		map.addAttribute("pages", page.getTotalPages());
		return "index";
	}

	@PostMapping("/save")
	public String saveData(@RequestParam final Integer id, @RequestParam final String authors,
			@RequestParam final String header, @RequestParam final String edition, @RequestParam final String date,
			@RequestParam final String description, @RequestParam final String keywords,
			@RequestParam final String source, @RequestParam final String annot, @RequestParam final String issn,
			@RequestParam final String language, @RequestParam final Integer collectionId, ModelMap map) {
		try {
			DocumentWrapper documentWrapper = new DocumentWrapper(documentService.readEager(id));
			documentWrapper.setAuthors(authors);
			documentWrapper.setDescription(description);
			documentWrapper.setHeader(header);
			documentWrapper.setEdition(edition);
			documentWrapper.setDate(date);
			documentWrapper.setKeywords(keywords);
			documentWrapper.setSource(source);
			documentWrapper.setLanguage(language);
			// documentWrapper.setRubric(rubric);
			documentWrapper.setIssn(issn);
			documentWrapper.setAnnotation(annot);
			String result = documentExporter.export(documentWrapper, collectionId);
			map.addAttribute("result", result);
			return "result";
		} catch (Exception e) {
			map.addAttribute("cause", "Невозможно импортировать документ");
			map.addAttribute("exception", e);
			return "error";
		}
	}

}