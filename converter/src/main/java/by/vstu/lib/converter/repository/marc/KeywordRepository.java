package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Keyword;

public interface KeywordRepository extends JpaRepository<Keyword, Integer> {

	List<Keyword> findByName(String name);

}
