package by.vstu.lib.converter.entity;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Author;
import by.vstu.lib.converter.entity.marc.Document;
import by.vstu.lib.converter.entity.marc.Header;
import by.vstu.lib.converter.entity.marc.Keyword;
import by.vstu.lib.converter.entity.marc.Language;
import by.vstu.lib.converter.entity.marc.Source;
import by.vstu.lib.converter.entity.marc.Url;
import by.vstu.lib.converter.util.exporter.util.DescriptionCreator;

public class DocumentWrapper {

	private Document document;
	private String description;
	private String annot;
	private String issn;
	private String date;
	private String edition;

	private Long collectionId;

	public DocumentWrapper(Document document) {
		this.document = document;
		description = DescriptionCreator.createDesription(document);
		splitDateAndEdition();
	}

	private void splitDateAndEdition() {
		String name = document.getEditions().get(0).getName();
		if (name.contains(",")) {
			String[] dateAndEdition = name.split(",");
			edition = dateAndEdition[0];
			date = dateAndEdition[1];
		} else {
			date = name;
		}
	}

	public String getAuthors() {
		StringBuilder builder = new StringBuilder();
		for (Author author : document.getAuthors()) {
			builder.append(author.getName());
			builder.append(";");
		}
		builder.setLength(builder.length() - 1);
		return builder.toString();
	}

	public void setAuthors(String authors) {
		String[] newAuthors = authors.split("\\s*;\\s*");
		List<Author> oldAuthors = document.getAuthors();
		oldAuthors.clear();
		for (String authorName : newAuthors) {
			Author author = new Author();
			author.setName(authorName);
			oldAuthors.add(author);
		}
	}

	public String getHeader() {
		return document.getHeaders().get(0).getName();
	}

	public void setHeader(String headerName) {
		document.getHeaders().clear();
		Header header = new Header();
		header.setName(headerName);
		document.getHeaders().add(header);
	}

	public String getKeywords() {
		StringBuilder builder = new StringBuilder();
		for (Keyword keyword : document.getKeywords()) {
			builder.append(keyword.getName());
			builder.append(";");
		}
		builder.setLength(builder.length() - 1);
		return builder.toString();
	}

	public void setKeywords(String keywords) {
		String[] newKeywords = keywords.split("\\s*;\\s*");
		List<Keyword> oldKeywords = document.getKeywords();
		oldKeywords.clear();
		for (String keywordName : newKeywords) {
			Keyword keyword = new Keyword();
			keyword.setName(keywordName);
			oldKeywords.add(keyword);
		}
	}

	public String getSource() {
		return document.getSources().get(0).getName();
	}

	public void setSource(String sourceName) {
		document.getSources().clear();
		Source source = new Source();
		source.setName(sourceName);
		document.getSources().add(source);
	}

	public String getLanguage() {
		return document.getLanguages().get(0).getName();
	}

	public void setLanguage(String languageName) {
		document.getLanguages().clear();
		Language language = new Language();
		language.setName(languageName);
		document.getLanguages().add(language);
	}

	public String getUrl() {
		return document.getUrls().get(0).getName();
	}

	public void setUrl(String urlName) {
		document.getUrls().clear();
		Url url = new Url();
		url.setName(urlName);
		document.getUrls().add(url);
	}

	// public String getRubric() {
	// return document.getRubrics().get(0).getName();
	// }

	// public void setRubric(String rubricName) {
	// document.getRubrics().clear();
	// Rubric rubric = new Rubric();
	// rubric.setName(rubricName);
	// document.getRubrics().add(rubric);
	// }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAnnotation() {
		return annot;
	}

	public void setAnnotation(String annot) {
		this.annot = annot;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public Document getDocument() {
		return document;
	}

	public Long getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(Long collectionId) {
		this.collectionId = collectionId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

}
