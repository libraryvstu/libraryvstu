package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Bibliography;
import by.vstu.lib.converter.service.Service;

public interface BibliographyService extends Service<Bibliography> {

	List<Bibliography> readByName(String name);

}