package by.vstu.lib.converter.util.exporter.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasswordHash {
	private static final Logger log = LoggerFactory.getLogger(PasswordHash.class);
	private static final Charset UTF_8 = Charset.forName("UTF-8"); // Should always succeed: UTF-8 is required

	private static final String DEFAULT_DIGEST_ALGORITHM = "SHA-512"; // XXX magic
	private static final int HASH_ROUNDS = 1024; // XXX magic 1024 rounds

	private String algorithm;
	private byte[] salt;
	private byte[] hash;

	/** Don't allow empty instances. */
	private PasswordHash() {
	}

	public PasswordHash(String algorithm, byte[] salt, byte[] hash) {
		if ((null != algorithm) && algorithm.isEmpty())
			this.algorithm = null;
		else
			this.algorithm = algorithm;

		this.salt = salt;

		this.hash = hash;
	}

	public PasswordHash(String algorithm, String salt, String hash) throws DecoderException {
		if ((null != algorithm) && algorithm.isEmpty())
			this.algorithm = null;
		else
			this.algorithm = algorithm;

		if (null == salt)
			this.salt = null;
		else
			this.salt = Hex.decodeHex(salt.toCharArray());

		if (null == hash)
			this.hash = null;
		else
			this.hash = Hex.decodeHex(hash.toCharArray());
	}

	public boolean matches(String secret) {
		byte[] candidate;
		try {
			candidate = digest(salt, algorithm, secret);
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage());
			return false;
		}
		return Arrays.equals(candidate, hash);
	}

	public byte[] getHash() {
		return hash;
	}

	public String getHashString() {
		if (null != hash)
			return new String(Hex.encodeHex(hash));
		else
			return null;
	}

	public byte[] getSalt() {
		return salt;
	}

	public String getSaltString() {
		if (null != salt)
			return new String(Hex.encodeHex(salt));
		else
			return null;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	static public String getDefaultAlgorithm() {
		return DEFAULT_DIGEST_ALGORITHM;
	}

	public static byte[] digest(byte[] salt, String algorithm, String secret) throws NoSuchAlgorithmException {
		MessageDigest digester;

		if (null == secret)
			secret = "";

		// Special case: old unsalted one-trip MD5 hash.
		if (null == algorithm) {
			digester = MessageDigest.getInstance("MD5");
			digester.update(secret.getBytes(UTF_8));
			return digester.digest();
		}

		// Set up a digest
		digester = MessageDigest.getInstance(algorithm);

		// Grind up the salt with the password, yielding a hash
		if (null != salt)
			digester.update(salt);

		digester.update(secret.getBytes(UTF_8)); // Round 0

		for (int round = 1; round < HASH_ROUNDS; round++) {
			byte[] lastRound = digester.digest();
			digester.reset();
			digester.update(lastRound);
		}

		return digester.digest();
	}
}
