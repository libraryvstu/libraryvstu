package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.vstu.lib.converter.entity.marc.Document;
import by.vstu.lib.converter.repository.marc.DocumentRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.DocumentService;

@Service
public class DocumentServiceImpl extends AbstractService<Document> implements DocumentService {

	@Autowired
	private DocumentRepository repository;

	@Override
	protected JpaRepository<Document, Integer> getRepository() {
		return repository;
	}

	@Transactional(readOnly = true)
	@Override
	public Document readEager(Integer id) {
		Document document = repository.findOne(id);
		if (document != null) {
			Hibernate.initialize(document.getUrls());
			Hibernate.initialize(document.getAuthors());
			Hibernate.initialize(document.getEditions());
			Hibernate.initialize(document.getHeaders());
			Hibernate.initialize(document.getKeywords());
			Hibernate.initialize(document.getLanguages());
			Hibernate.initialize(document.getSources());
			Hibernate.initialize(document.getPages());
			Hibernate.initialize(document.getBibliography());
		}
		return document;
	}

	@Override
	public List<Document> findByRectype(Character rectype) {
		return repository.findByRectype(rectype);
	}

	@Override
	public List<Document> findByBiblevel(Character biblevel) {
		return repository.findByBiblevel(biblevel);
	}

	@Transactional(readOnly = true)
	@Override
	public Page<Document> readWithSourcesAndHeaders(PageRequest request) {
		Page<Document> documents = repository.findAll(request);
		for (Document document : documents) {
			Hibernate.initialize(document.getSources());
			Hibernate.initialize(document.getHeaders());
		}
		return documents;
	}

	@Transactional(readOnly = true)
	@Override
	public Page<Document> findBySourceNameContainingWithSources(final String name, PageRequest pageRequest) {
		Page<Document> page = repository.findAllBySourcesNameContainingIgnoreCaseOrHeadersNameContainingIgnoreCase(name,
				name, pageRequest);
		for (Document document : page.getContent()) {
			Hibernate.initialize(document.getSources());
			Hibernate.initialize(document.getHeaders());
		}
		return page;
	}

}
