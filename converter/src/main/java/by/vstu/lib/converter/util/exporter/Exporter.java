package by.vstu.lib.converter.util.exporter;

import java.io.File;

import by.vstu.lib.converter.entity.DocumentWrapper;

public interface Exporter {

	void export(DocumentWrapper documentWrapper, File tempDir);

}
