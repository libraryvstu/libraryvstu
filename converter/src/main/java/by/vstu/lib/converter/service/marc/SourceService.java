package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Source;
import by.vstu.lib.converter.service.Service;

public interface SourceService extends Service<Source> {

	List<Source> readByName(String name);

}
