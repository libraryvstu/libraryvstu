package by.vstu.lib.converter.service.impl.dspace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.dspace.Meta;
import by.vstu.lib.converter.repository.dspace.MetaRepository;
import by.vstu.lib.converter.service.dspace.MetaService;
import by.vstu.lib.converter.service.impl.AbstractService;

@Service
public class MetaServiceImpl extends AbstractService<Meta> implements MetaService {

	@Autowired
	private MetaRepository repository;

	@Override
	protected JpaRepository<Meta, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Meta> readCollections() {
		return repository.findByType(Meta.COLLECTION);
	}

}
