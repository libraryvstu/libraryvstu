package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Pages;
import by.vstu.lib.converter.repository.marc.PagesRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.PagesService;

@Service
public class PagesServiceImpl extends AbstractService<Pages> implements PagesService {

	@Autowired
	private PagesRepository repository;

	@Override
	protected JpaRepository<Pages, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Pages> readByName(String name) {
		return repository.findByName(name);
	}

}
