package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Edition;

public interface EditionRepository extends JpaRepository<Edition, Integer> {

	List<Edition> findByName(String name);

}
