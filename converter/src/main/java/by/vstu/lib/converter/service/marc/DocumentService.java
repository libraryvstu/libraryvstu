package by.vstu.lib.converter.service.marc;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import by.vstu.lib.converter.entity.marc.Document;
import by.vstu.lib.converter.service.Service;

public interface DocumentService extends Service<Document> {

	Document readEager(Integer id);

	List<Document> findByRectype(Character rectype);

	List<Document> findByBiblevel(Character biblevel);

	Page<Document> readWithSourcesAndHeaders(PageRequest request);

	Page<Document> findBySourceNameContainingWithSources(String name, PageRequest request);

}
