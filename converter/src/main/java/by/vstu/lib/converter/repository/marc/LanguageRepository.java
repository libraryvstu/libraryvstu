package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Language;

public interface LanguageRepository extends JpaRepository<Language, Integer> {

	List<Language> findByName(String name);

}
