package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Keyword;
import by.vstu.lib.converter.service.Service;

public interface KeywordService extends Service<Keyword> {

	List<Keyword> readByName(String name);

}
