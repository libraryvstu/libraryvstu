package by.vstu.lib.converter.service;

import java.util.List;

import by.vstu.lib.converter.entity.Identified;

public interface Service<T extends Identified> {

	T read(Integer id);

	List<T> read();

	void save(T entity);

	void delete(Integer id);

}
