package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Language;
import by.vstu.lib.converter.repository.marc.LanguageRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.LanguageService;

@Service
public class LanguageServiceImpl extends AbstractService<Language> implements LanguageService {

	@Autowired
	private LanguageRepository repository;

	@Override
	protected JpaRepository<Language, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Language> readByName(String name) {
		return repository.findByName(name);
	}

}
