package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Document;

public interface DocumentRepository extends JpaRepository<Document, Integer> {

	List<Document> findByRectype(Character rectype);

	List<Document> findByBiblevel(Character biblevel);

	List<Document> findByItem(String item);

	Page<Document> findAllBySourcesNameContainingIgnoreCaseOrHeadersNameContainingIgnoreCase(String name1, String name2,
			Pageable pageable);

}
