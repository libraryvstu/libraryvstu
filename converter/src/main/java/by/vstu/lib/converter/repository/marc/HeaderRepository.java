package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Header;

public interface HeaderRepository extends JpaRepository<Header, Integer> {

	List<Header> findByName(String name);

}
