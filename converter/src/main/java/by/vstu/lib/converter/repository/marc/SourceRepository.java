package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Source;

public interface SourceRepository extends JpaRepository<Source, Integer> {

	List<Source> findByName(String name);

}
