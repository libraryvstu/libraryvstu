package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Source;
import by.vstu.lib.converter.repository.marc.SourceRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.SourceService;

@Service
public class SourceServiceImpl extends AbstractService<Source> implements SourceService {

	@Autowired
	private SourceRepository repository;

	@Override
	protected JpaRepository<Source, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Source> readByName(String name) {
		return repository.findByName(name);
	}

}
