package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Author;
import by.vstu.lib.converter.service.Service;

public interface AuthorService extends Service<Author> {

	List<Author> readByName(String name);

}
