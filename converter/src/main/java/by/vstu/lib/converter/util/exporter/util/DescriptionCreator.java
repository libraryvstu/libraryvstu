/**
 * Класс формирования библиографического описания.
 * @author Ivanova Inna 
 
 */
package by.vstu.lib.converter.util.exporter.util;

import java.util.Iterator;
import java.util.List;

import by.vstu.lib.converter.entity.marc.Author;
import by.vstu.lib.converter.entity.marc.Bibliography;
import by.vstu.lib.converter.entity.marc.Document;

public class DescriptionCreator {

	public static String createDesription(Document document) {
		StringBuilder sb = new StringBuilder();
		String authorName = AuthorCreator.getFirstAuthor(document.getItem());

		Iterator<Author> iterator = document.getAuthors().iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getName().equals(authorName)) {
				iterator.remove();
				break;
			}
		}

		/** Библиографичекое описание */
		if (document.getAuthors().size() < 4) {
			Author author = new Author();
			author.setName(authorName);
			document.getAuthors().add(0, author);

			sb.append(authorName);
			sb.append(" ");
		}
		sb.append(document.getHeaders().get(0).getName());
		sb.append(" / ");
		appendAuthors(document.getAuthors(), sb);
		sb.setLength(sb.length() - 1);
		sb.append(" // ");
		sb.append(document.getSources().get(0).getName());
		sb.append(". - ");
		sb.append(document.getEditions().get(0).getName());
		sb.append(". - ");
		sb.append(document.getPages().get(0).getName());
		appendBibliography(document.getBibliography(), sb);
		return sb.toString();
	}

	private static void appendBibliography(List<Bibliography> bibliographies, StringBuilder sb) {
		boolean empty = false;
		if (!bibliographies.isEmpty()) {
			String name = bibliographies.get(0).getName();
			// System.out.println(name);
			if (name == null || name.trim().equals("<null>")) {
				empty = true;
			}
		} else {
			empty = true;
		}
		if (!empty) {
			sb.append(bibliographies.get(0).getName());
		} else {
			sb.append(".");
		}
	}

	private static void appendAuthors(List<Author> authors, StringBuilder sb) {
		if (authors.size() >= 4) {
			handleManyAuthors(authors, sb);
		} else {
			handleFewAuthors(authors, sb);
		}
	}

	private static void handleManyAuthors(List<Author> authors, StringBuilder sb) {
		handleAuthor(authors.get(0), sb);
		sb.append("[и др.] ");
	}

	private static void handleAuthor(Author author, StringBuilder sb) {
		String name = author.getName();
		int spaceIndex = name.indexOf(" ");
		sb.append(name.substring(spaceIndex, name.length()));
		sb.append(" ");
		sb.append(name.substring(0, spaceIndex + 1));
	}

	private static void handleFewAuthors(List<Author> authors, StringBuilder sb) {
		for (Author author : authors) {
			handleAuthor(author, sb);
		}
	}

}
