package by.vstu.lib.converter.entity.dspace;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import by.vstu.lib.converter.entity.Identified;

@Entity
@Table(name = "eperson", schema = "public")
public class Role implements Identified {

	@Id
	@Column(name = "eperson_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "epersongroup2eperson", schema = "public", joinColumns = {
			@JoinColumn(name = "eperson_group_id") }, inverseJoinColumns = { @JoinColumn(name = "epersen_id") })
	private List<User> users;

	public String getRoleName() {
		if (id == 1) {
			return "ROLE_ADMIN";
		}
		return "ROLE_USER";
	}

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", users=" + users + "]";
	}

}