package by.vstu.lib.converter.util.exporter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import by.vstu.lib.converter.entity.DocumentWrapper;
import by.vstu.lib.converter.entity.marc.Document;

class ContentsExporter implements Exporter {

	@Override
	public void export(DocumentWrapper documentWrapper, File dir) {
		Document document = documentWrapper.getDocument();
		try {

			File folder = new File(dir, document.getId().toString());

			FileWriter writer = new FileWriter(new File(folder, "contents"));

			for (String file : folder.list()) {
				if (file.endsWith(".pdf")) {
					writer.write(file + "\tbundle:ORIGINAL\r\n");
				}
			}

			writer.write("license.txt\tbundle:LICENSE");
			writer.close();

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
