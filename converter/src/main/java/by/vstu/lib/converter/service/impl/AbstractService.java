package by.vstu.lib.converter.service.impl;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.Identified;
import by.vstu.lib.converter.entity.marc.Document;
import by.vstu.lib.converter.service.Service;

public abstract class AbstractService<T extends Identified> implements Service<T> {

	protected abstract JpaRepository<T, Integer> getRepository();

	@Override
	public T read(Integer id) {
		return getRepository().findOne(id);
	}

	@Override
	public List<T> read() {
		return getRepository().findAll();
	}

	@Override
	public void save(T entity) {
		getRepository().save(entity);
	}

	@Override
	public void delete(Integer id) {
		getRepository().delete(id);
	}

	public List<Document> findBySourceNameContaining(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
