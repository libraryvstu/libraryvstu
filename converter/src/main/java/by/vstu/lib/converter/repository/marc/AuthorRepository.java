package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Author;

public interface AuthorRepository extends JpaRepository<Author, Integer> {

	List<Author> findByName(String name);

}
