package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Keyword;
import by.vstu.lib.converter.repository.marc.KeywordRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.KeywordService;

@Service
public class KeywordServiceImpl extends AbstractService<Keyword> implements KeywordService {

	@Autowired
	private KeywordRepository repository;

	@Override
	protected JpaRepository<Keyword, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Keyword> readByName(String name) {
		return repository.findByName(name);
	}

}