package by.vstu.lib.converter.repository.dspace;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.dspace.Meta;

public interface MetaRepository extends JpaRepository<Meta, Integer> {

	List<Meta> findByType(Integer type);

}
