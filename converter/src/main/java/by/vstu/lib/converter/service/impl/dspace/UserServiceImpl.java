package by.vstu.lib.converter.service.impl.dspace;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.vstu.lib.converter.entity.dspace.User;
import by.vstu.lib.converter.repository.dspace.UserRepository;
import by.vstu.lib.converter.service.dspace.UserService;
import by.vstu.lib.converter.service.impl.AbstractService;

@Service
public class UserServiceImpl extends AbstractService<User> implements UserService {

	@Autowired
	private UserRepository repository;

	@Override
	protected JpaRepository<User, Integer> getRepository() {
		return repository;
	}

	@Transactional(readOnly = true)
	@Override
	public User readByLogin(String login) {
		User user = repository.findByLogin(login);
		Hibernate.initialize(user.getRoles());
		return user;
	}

}
