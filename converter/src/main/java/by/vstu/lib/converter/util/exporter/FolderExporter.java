package by.vstu.lib.converter.util.exporter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import javax.servlet.ServletContext;

import by.vstu.lib.converter.entity.DocumentWrapper;
import by.vstu.lib.converter.entity.marc.Document;

class FolderExporter implements Exporter {

	private ServletContext servletContext;

	@Override
	public void export(DocumentWrapper documentWrapper, File dir) {
		Document document = documentWrapper.getDocument();
		try {
			File folder = new File(dir, document.getId().toString());
			if (!folder.exists()) {
				folder.mkdir();
			}

			File source = new File(servletContext.getRealPath("/WEB-INF/classes/license.txt"));
			File dest = new File(folder, "license.txt");
			try {
				Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				e.printStackTrace();
			}

			String href = document.getUrls().get(0).getName();
			href = href.substring(href.indexOf("http"), href.indexOf("pdf") + 3);
			String name = href.substring(href.lastIndexOf("/") + 1, href.length());

			URL website = new URL(href.replace("lib.vstu.by", "192.168.20.245").replaceAll(" ", "%20"));
			try (InputStream in = website.openStream()) {
				Files.copy(in, new File(folder, name).toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}
