package by.vstu.lib.converter.util.exporter;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import by.vstu.lib.converter.entity.DocumentWrapper;
import by.vstu.lib.converter.entity.marc.Document;

class DublinExporter implements Exporter {

	@Override
	public void export(DocumentWrapper documentWrapper, File dir) {
		Document document = documentWrapper.getDocument();
		XmlTemplate teplate = new XmlTemplate();
		document.getAuthors().stream().forEach(author -> {
			teplate.addItem("contributor", "author", author.getName());
		});
		teplate.addItem("publisher", "none", "ru-RU", "Витебский государственный технологический университет");

		teplate.addItem("date", "issued", documentWrapper.getDate());
		teplate.addItem("identifier", "citation", "ru-RU", documentWrapper.getDescription());

		teplate.addItem("identifier", "issn", "ru-RU", documentWrapper.getIssn());

		document.getKeywords().stream().forEach(keyword -> {
			teplate.addItem("description", "none", keyword.getName());
		});

		teplate.addItem("description", "abstract", documentWrapper.getAnnotation());

		document.getLanguages().stream().forEach(language -> {
			teplate.addItem("language", "iso", "ru-RU", language.getName());
		});

		document.getSources().stream().forEach(source -> {
			teplate.addItem("source", "none", "ru-RU", source.getName());
		});

		document.getHeaders().stream().forEach(header -> {
			teplate.addItem("title", "none", "ru-RU", header.getName());
		});

		teplate.addItem("format", "mimetype", "ru-RU", "application/pdf");
		teplate.addItem("type", "none", "ru-RU", "Article");

		try {
			File file = new File(dir, document.getId() + "/dublin_core.xml");
			JAXBContext context = JAXBContext.newInstance(XmlTemplate.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(teplate, file);
		} catch (JAXBException ex) {
			Logger.getLogger(DublinExporter.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
