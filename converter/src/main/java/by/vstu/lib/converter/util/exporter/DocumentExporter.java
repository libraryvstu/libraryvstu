package by.vstu.lib.converter.util.exporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.lib.converter.entity.DocumentWrapper;

@Component
public class DocumentExporter {

	private static final String DSPACE_IMPORTER = "C:\\DSpace\\bin\\dspace.bat";
	private static final String DSPACE_USER = "admin@localhost";

	private List<Exporter> EXPORTERS = new ArrayList<>();

	@Autowired
	private ServletContext servletContext;

	@PostConstruct
	public void init() {
		FolderExporter exporter = new FolderExporter();
		exporter.setServletContext(servletContext);
		EXPORTERS.add(exporter);
		EXPORTERS.add(new ContentsExporter());
		EXPORTERS.add(new DublinExporter());
	}

	public String export(DocumentWrapper document, Integer collectionId) {
		try {
			return exportDocs(document, collectionId);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public String exportDocs(DocumentWrapper document, Integer collectionId) throws IOException {
		File tempDir = Files.createTempDirectory("export").toFile();
		for (Exporter exporter : EXPORTERS) {
			exporter.export(document, tempDir);
		}
		String result = importDocs(collectionId, tempDir);
		delete(tempDir);
		return result;
	}

	private String importDocs(Integer collectionId, File temp) throws IOException {
		String execution = prepareExecution(collectionId, temp);
		Process process = Runtime.getRuntime().exec(execution);
		return getExecutionResult(process);
	}

	private String prepareExecution(Integer collectionId, File temp) {
		String filepath = temp.getAbsolutePath();
		String mapfile = new File(temp, "mapfile").getAbsolutePath();
		String execution = String.format("%s import -a -e %s -c  %d -s %s -m %s", DSPACE_IMPORTER, DSPACE_USER,
				collectionId, filepath, mapfile);
		return execution;
	}

	private String getExecutionResult(Process process) throws IOException {
		String line;
		StringBuilder sb = new StringBuilder();
		BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
		while ((line = input.readLine()) != null) {
			sb.append(line).append("<br>");
		}
		input.close();
		BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		while ((line = error.readLine()) != null) {
			sb.append(line).append("<br>");
		}
		error.close();
		return sb.toString();
	}

	static void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles()) {
				delete(c);
			}
		}
		if (!f.delete()) {
			throw new FileNotFoundException("Failed to delete file: " + f);
		}
	}

}
