package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Language;
import by.vstu.lib.converter.service.Service;

public interface LanguageService extends Service<Language> {

	List<Language> readByName(String name);
}
