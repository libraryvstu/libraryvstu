package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Edition;
import by.vstu.lib.converter.service.Service;

public interface EditionService extends Service<Edition> {

	List<Edition> readByName(String name);

}
