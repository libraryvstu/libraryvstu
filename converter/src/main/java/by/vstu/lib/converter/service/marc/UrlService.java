package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Url;
import by.vstu.lib.converter.service.Service;

public interface UrlService extends Service<Url> {

	List<Url> readByName(String name);

}
