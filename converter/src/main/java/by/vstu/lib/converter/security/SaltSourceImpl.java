package by.vstu.lib.converter.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import by.vstu.lib.converter.service.dspace.UserService;

@Component
public class SaltSourceImpl implements SaltSource {

	@Autowired
	private UserService userService;

	@Override
	public Object getSalt(UserDetails userDetails) {
		String username = userDetails.getUsername();
		return userService.readByLogin(username).getSalt();
	}

}
