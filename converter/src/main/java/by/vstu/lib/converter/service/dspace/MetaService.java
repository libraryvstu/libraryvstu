package by.vstu.lib.converter.service.dspace;

import java.util.List;

import by.vstu.lib.converter.entity.dspace.Meta;
import by.vstu.lib.converter.service.Service;

public interface MetaService extends Service<Meta> {

	List<Meta> readCollections();

}