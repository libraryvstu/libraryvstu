package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Edition;
import by.vstu.lib.converter.repository.marc.EditionRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.EditionService;

@Service
public class EditionServiceImpl extends AbstractService<Edition> implements EditionService {

	@Autowired
	private EditionRepository repository;

	@Override
	protected JpaRepository<Edition, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Edition> readByName(String name) {
		return repository.findByName(name);
	}
}
