package by.vstu.lib.converter.security;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

@SuppressWarnings("deprecation")
public class PasswordEncoderImpl implements org.springframework.security.authentication.encoding.PasswordEncoder {

	private static final Charset UTF_8 = Charset.forName("UTF-8");
	private static final int HASH_ROUNDS = 1024; // XXX magic 1024 rounds

	@Override
	public String encodePassword(String password, Object salt) {
		try {
			byte[] digest = digest(salt, "SHA-512", password);
			return new String(Hex.encodeHex(digest));
		} catch (NoSuchAlgorithmException | DecoderException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isPasswordValid(String encPass, String pass, Object salt) {
		return encodePassword(pass, salt).equals(encPass);
	}

	private byte[] digest(Object salt, String algorithm, String secret)
			throws NoSuchAlgorithmException, DecoderException {
		MessageDigest digester;

		if (null == secret)
			secret = "";

		// Special case: old unsalted one-trip MD5 hash.
		if (null == algorithm) {
			digester = MessageDigest.getInstance("MD5");
			digester.update(secret.getBytes(UTF_8));
			return digester.digest();
		}

		// Set up a digest
		digester = MessageDigest.getInstance(algorithm);

		// Grind up the salt with the password, yielding a hash
		if (null != salt) {
			byte[] hex = Hex.decodeHex(((String) salt).toCharArray());
			digester.update(hex);
		}

		digester.update(secret.getBytes(UTF_8)); // Round 0

		for (int round = 1; round < HASH_ROUNDS; round++) {
			byte[] lastRound = digester.digest();
			digester.reset();
			digester.update(lastRound);
		}

		return digester.digest();
	}

}
