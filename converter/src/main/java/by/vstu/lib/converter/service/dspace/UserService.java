package by.vstu.lib.converter.service.dspace;

import by.vstu.lib.converter.entity.dspace.User;
import by.vstu.lib.converter.service.Service;

public interface UserService extends Service<User> {

	User readByLogin(String login);

}