package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Url;
import by.vstu.lib.converter.repository.marc.UrlRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.UrlService;

@Service
public class UrlServiceImpl extends AbstractService<Url> implements UrlService {

	@Autowired
	private UrlRepository repository;

	@Override
	protected JpaRepository<Url, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Url> readByName(String name) {
		return repository.findByName(name);
	}

}
