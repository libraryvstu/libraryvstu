/**
 * Класс поиска совпадений в стоке Item базы Марк.
 * @author Ivanova Inna 
 
 */
package by.vstu.lib.converter.util.exporter.util;

public class AuthorCreator {
	/**
	 * Поверка строк на совпадние ( если в каком нибудь из случаев совпадает, то
	 * обрезаем автора)
	 */
	public static String getFirstAuthor(String item) {

		int authorIndex = item.indexOf("24500");
		String author;
		if (authorIndex > 0) {
			author = item.substring(0, authorIndex - 1);
		} else {
			author = item.substring(0, item.indexOf("245") - 1);
		}

		try {
			int index10011 = author.indexOf("10011");
			int index10010 = author.indexOf("10010");
			int index1001 = author.indexOf("1001");
			int index100 = author.indexOf("100");
			int index710 = author.indexOf("710");

			if (index10011 > 0) {
				author = author.substring(index10011 + 7, author.length());

			} else if (index10010 > 0) {
				author = author.substring(index10010 + 7, author.length());
				int index7102 = author.indexOf("710");
				if (index7102 > 0) {
					author = author.substring(index7102 + 5, author.length());
				}
			} else if (index1001 > 0) {
				author = author.substring(index1001 + 7, author.length());
				int index7103 = author.indexOf("710");
				if (index7103 > 0) {
					author = author.substring(0, index7103 - 1);
				}
			} else if (index100 > 0) {
				author = author.substring(index100 + 7, author.length());
			} else {
				author = author.substring(index710 + 5, author.length());
			}

			int indexC = author.lastIndexOf("c");
			if (indexC > 0) {
				author = author.substring(0, indexC - 1);
			}

			String name = author.substring(author.lastIndexOf("c") + 1, author.length());
			return name;
		} catch (Exception e) {
			return author;
		}
	}
}
