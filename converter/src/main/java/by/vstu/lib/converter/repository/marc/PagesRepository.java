package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Pages;

public interface PagesRepository extends JpaRepository<Pages, Integer> {

	List<Pages> findByName(String name);

}
