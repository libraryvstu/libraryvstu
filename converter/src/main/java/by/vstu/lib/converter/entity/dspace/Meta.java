package by.vstu.lib.converter.entity.dspace;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import by.vstu.lib.converter.entity.Identified;

@Entity
@Table(name = "metadatavalue", schema = "public")
public class Meta implements Identified {

	public static final int COLLECTION = 3;

	@Id
	@Column(name = "metadata_value_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "text_value")
	private String value;

	@Column(name = "resource_type_id")
	private Integer type;

	@Column(name = "resource_id")
	private Integer resource;

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getType() {
		return type;
	}

	public Integer getResource() {
		return resource;
	}

	public void setResource(Integer resource) {
		this.resource = resource;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
