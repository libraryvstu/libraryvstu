package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Bibliography;

public interface BibliographyRepository extends JpaRepository<Bibliography, Integer> {

	List<Bibliography> findByName(String name);

}
