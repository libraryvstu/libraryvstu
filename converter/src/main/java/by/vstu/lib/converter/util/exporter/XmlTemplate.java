package by.vstu.lib.converter.util.exporter;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "dublin_core")
class XmlTemplate {

	@XmlAttribute(name = "schema")
	public static final String schema = "dc";

	@XmlElement(name = "dcvalue")
	private List<Value> values = new ArrayList<>();

	public void addItem(String element, String qualifier, String value) {
		values.add(new Value(element, qualifier, value));
	}

	public void addItem(String element, String qualifier, String language, String value) {
		values.add(new Value(element, qualifier, language, value));
	}

	private static class Value {

		@XmlAttribute(name = "element")
		private String element;

		@XmlAttribute(name = "qualifier")
		private String qualifier;

		@XmlAttribute(name = "language")
		private String language;

		@XmlValue
		private String value;

		public Value(String element, String qualifier, String value) {
			this.element = element;
			this.qualifier = qualifier;
			this.value = value;
		}

		public Value(String element, String qualifier, String language, String value) {
			this.element = element;
			this.qualifier = qualifier;
			this.language = language;
			this.value = value;
		}

	}

}