package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Bibliography;
import by.vstu.lib.converter.repository.marc.BibliographyRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.BibliographyService;

@Service
public class BibliographyServiceImpl extends AbstractService<Bibliography> implements BibliographyService {

	@Autowired
	private BibliographyRepository repository;

	@Override
	protected JpaRepository<Bibliography, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Bibliography> readByName(String name) {
		return repository.findByName(name);
	}

}
