package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Header;
import by.vstu.lib.converter.service.Service;

public interface HeaderService extends Service<Header> {

	List<Header> readByName(String name);

}
