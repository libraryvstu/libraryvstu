package by.vstu.lib.converter.entity;

public interface Identified {

	Integer getId();

}
