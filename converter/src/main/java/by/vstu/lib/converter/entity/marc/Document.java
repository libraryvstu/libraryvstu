/**
 * Класс   формирования документа.
 * @author Ivanova Inna 
 
 */

package by.vstu.lib.converter.entity.marc;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import by.vstu.lib.converter.entity.Identified;

/** Название поля в Марк */
@Entity
@Table(name = "DOC", schema = "dbo")
public class Document implements Identified {
	/** Поле таблицы документа */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DOC_ID")
	private Integer id;
	@Column
	private Character rectype;

	@Column
	private Character biblevel;

	@Column(columnDefinition = "text")
	private String item;
	/** Связь с таблицами автора */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX100aX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Author> authors;
	/** Связь с таблицами дата издания */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX773dX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Edition> editions;
	/** Связь с таблицами заголовка */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX245aX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Header> headers;
	/** Связь с таблицами ключевых слов */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX653aX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Keyword> keywords;
	/** Связь с таблицами языка */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX041aX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Language> languages;
	/** Связь с таблицами названия источника */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX773tX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Source> sources;
	/** Связь с таблицами ссылки на источник */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX856lX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Url> urls;
	/** Связь с таблицами "Прочая информация" */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX773gX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Pages> pages;
	/** Связь с таблицами библиографии */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX500aX", schema = "dbo", joinColumns = { @JoinColumn(name = "DOC_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "IDX_ID") })
	private List<Bibliography> bibliographys;

	/**
	 * Функция получения значения поля
	 * 
	 * @return возвращается id
	 */
	@Override
	public Integer getId() {
		return id;
	}

	/**
	 * Процедура опредления поля даты издания
	 * 
	 * @param id
	 * 
	 */

	public void setId(Integer id) {
		this.id = id;
	}

	public Character getRectype() {
		return rectype;
	}

	public void setRectype(Character rectype) {
		this.rectype = rectype;
	}

	public Character getBiblevel() {
		return biblevel;
	}

	public void setBiblevel(Character biblevel) {
		this.biblevel = biblevel;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<Source> getSources() {
		return sources;
	}

	public void setSources(List<Source> sources) {
		this.sources = sources;
	}

	public List<Url> getUrls() {
		return urls;
	}

	public void setUrls(List<Url> urls) {
		this.urls = urls;
	}

	public List<Keyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<Keyword> keywords) {
		this.keywords = keywords;
	}

	public List<Header> getHeaders() {
		return headers;
	}

	public void setHeaders(List<Header> headers) {
		this.headers = headers;
	}

	public List<Edition> getEditions() {
		return editions;
	}

	public void setEditions(List<Edition> editions) {
		this.editions = editions;
	}

	public List<Pages> getPages() {
		return pages;
	}

	public void setPages(List<Pages> pages) {
		this.pages = pages;
	}

	public List<Bibliography> getBibliography() {
		return bibliographys;
	}

	public void setBibliography(List<Bibliography> bibliographys) {
		this.bibliographys = bibliographys;
	}

	@Override
	public String toString() {
		return String.format("Document [rectype=%s, biblevel=%s, item=%s, getId()=%s]", id, rectype, biblevel, item);
	}

}
