package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Author;
import by.vstu.lib.converter.repository.marc.AuthorRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.AuthorService;

@Service
public class AuthorServiceImpl extends AbstractService<Author> implements AuthorService {

	@PostConstruct
	public void init() {
		System.out.println("initialization");
	}

	@Autowired
	private AuthorRepository repository;

	@Override
	protected JpaRepository<Author, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Author> readByName(String name) {
		return repository.findByName(name);
	}

}
