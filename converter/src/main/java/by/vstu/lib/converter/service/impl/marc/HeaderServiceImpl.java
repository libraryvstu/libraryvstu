package by.vstu.lib.converter.service.impl.marc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import by.vstu.lib.converter.entity.marc.Header;
import by.vstu.lib.converter.repository.marc.HeaderRepository;
import by.vstu.lib.converter.service.impl.AbstractService;
import by.vstu.lib.converter.service.marc.HeaderService;

@Service
public class HeaderServiceImpl extends AbstractService<Header> implements HeaderService {

	@Autowired
	private HeaderRepository repository;

	@Override
	protected JpaRepository<Header, Integer> getRepository() {
		return repository;
	}

	@Override
	public List<Header> readByName(String name) {
		return repository.findByName(name);
	}
}
