package by.vstu.lib.converter.repository.dspace;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.dspace.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByLogin(String login);

}
