package by.vstu.lib.converter.service.marc;

import java.util.List;

import by.vstu.lib.converter.entity.marc.Pages;
import by.vstu.lib.converter.service.Service;

public interface PagesService extends Service<Pages> {

	List<Pages> readByName(String name);

}
