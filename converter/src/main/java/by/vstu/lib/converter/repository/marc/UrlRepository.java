package by.vstu.lib.converter.repository.marc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import by.vstu.lib.converter.entity.marc.Url;

public interface UrlRepository extends JpaRepository<Url, Integer> {

	List<Url> findByName(String name);

}
