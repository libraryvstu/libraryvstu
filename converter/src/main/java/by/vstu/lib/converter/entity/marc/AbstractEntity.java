/**
 * Класс id 
 * @author Ivanova Inna 
 
 */

package by.vstu.lib.converter.entity.marc;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import by.vstu.lib.converter.entity.Identified;

@MappedSuperclass
public abstract class AbstractEntity implements Identified {
	/** Поле id */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX_ID")
	private Integer id;

	/**
	 * Функция получения значения поля
	 * 
	 * @return возвращается id
	 */
	@Override
	public Integer getId() {
		return id;
	}

	/**
	 * Процедура опредления id
	 * 
	 * @param id
	 * 
	 */

	public void setId(Integer id) {
		this.id = id;
	}

}
