/**
 * Класс сслыки на статью со свойством  name.
 * @author Ivanova Inna 
 
 */
package by.vstu.lib.converter.entity.marc;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/** Название поля в Марк */
@Entity
@Table(name = "IDX856l", schema = "dbo")
public class Url extends AbstractEntity {
	/** Поле ссылки на статью */
	@Column(name = "TERM")
	private String name;
	/** Связь таблицы, с другими таблицами */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "IDX856lX", schema = "dbo", joinColumns = { @JoinColumn(name = "IDX_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "DOC_ID") })
	private List<Document> documents;

	/**
	 * Функция получения значения поля
	 * 
	 * @return возвращается ссылка на статью
	 */
	public String getName() {
		return name;
	}

	/**
	 * Процедура опредления статьи
	 * 
	 * @param name
	 *            - ссылка на статью
	 */

	public void setName(String name) {
		this.name = name;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@Override
	public String toString() {
		return String.format("Url [name=%s, getId()=%s]", name, getId());
	}

}