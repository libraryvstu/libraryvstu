<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Конвертер|Шаг 2</title>

<script src="${pageContext.request.contextPath}/js/jquery-1.7.2.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.placeholder.min.js"></script>
<script src="${pageContext.request.contextPath}/js/valid.js"></script>
<script src="${pageContext.request.contextPath}/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/js/selectFx.js"></script>

<link rel="shortcut icon" href="img//favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/normalize.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/demo.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/effect2.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/cs-select.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/cs-skin-overlay.css" />
</head>

<body class="content">
	<div id="ip-container" class="ip-container">
		<div class="ip-main">
			<h3>Витебский государственный технологический университет</h3>
			<h2>Конвертер Marc SQL в DSpace</h2>
			<br> <a
				style=" color:#000000; margin-left: 84%; text-decoration: underline"
				href="/converter/logout">Выйти </a>
			<hr>
			<br>
			<p>
				<i><b> Шаг 2 - Проверьте правильность данных</b></i>
			</p>
			<form method="post" onsubmit="return validate(this)" action="../save">
				<input name="id" type="hidden" value="${document.document.id}">
				<table align="center" width="70%">
					<tr>
						<td>Авторы:</td>

						<td><textarea
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								name="authors" rows="3" cols="60">${document.authors}</textarea>
					</tr>
					<tr>
						<td>Заголовок:</td>
						<td><textarea
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								name="header" rows="5" cols="60"> ${document.header}</textarea>
					</tr>
					<tr>
						<td>Дата издания:</td>
						<td><textarea name="date"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="3" cols="60">${document.date}</textarea>
					</tr>
					<tr>
						<td>Библиографичское описание:</td>
						<td><textarea name="description"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="7" cols="60"> ${document.description}</textarea>
					</tr>
					<tr>
						<td>Ключевые слова:</td>
						<td><textarea name="keywords"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="7" cols="60"> ${document.keywords }</textarea>
					</tr>
					<tr>
						<td>Название источника:</td>
						<td><textarea name="source"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="5" cols="60">${document.source}</textarea>
					</tr>
					<tr>
						<td>Издательство:</td>
						<td><textarea name="edition"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="2" cols="60">Витебский государственный технологический университет</textarea></td>

					</tr>
					<tr>

						<td>Аннотация:</td>
						<td><textarea name="annot"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="7" cols="60"></textarea></td>
					</tr>
					<tr>
						<td>ISSN:</td>
						<td><textarea name="issn"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="2" cols="60"> </textarea></td>
					</tr>
					<tr>
						<td>Тип документа:</td>
						<td><textarea name="article"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="2" cols="60" disabled>Article</textarea></td>

					</tr>
					<tr>
						<td>Формат документа:</td>
						<td><textarea name="formatdoc"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="2" cols="60" disabled>application/pdf</textarea></td>
					</tr>
					<tr>
						<td>Язык:</td>
						<td><textarea name="language"
								style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;"
								rows="2" cols="60">${document.language}</textarea>
					</tr>
					
					<tr>
						<td>Документ:</td>
						<td>${document.url}</td>
					</tr>

					<tr>
						<td style="text-decoration: underline; font-weight: 600;">Выберете коллекцию из DSpace:</td>
						<td><select class="cs-select cs-skin-overlay"
							id="collectionSel" name="collectionId" style="width: 95%"><option
									value="" disabledselected ">- Выберите из списка -</option>
								<c:forEach items="${collections}" var="collection">
									<option value="${collection.resource}">${collection.value}</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td>Показать исходные данные: <input type="checkbox"
							value="1" onclick="showMe(this)">
						</td>


						<td>
							<div id="div1" style="display: block; display: none;">
								<textarea rows="7" cols="60"
									style="resize: none; font-size: 12pt; margin: 5px; border-radius: 5px;">${document.document.item}</textarea>
							</div>
						</td>
					</tr>
				</table>
				<script type="text/javascript">
					function showMe(box) {
						var vis = (box.checked) ? "block" : "none";
						document.getElementById('div1').style.display = vis;
					}
				</script>

				<div class="button">
					<a  href="/converter/">Назад</a> <input
						type="submit" value="Импортировать">
				</div>
			</form>
			<br>
			<div id="error" class="error"
				style="color: red; text-align: center; padding: 19px; margin: 5px;">
				<!-- error message -->
			</div>
			<div class="ip-main"
				style="text-align: center; font-family: Century Gothic; font-size: 12pt; font-style: italic;">
				<hr>
				© Библиотека УО "ВГТУ", 2018 | Все права защищены

			</div>
		</div>
	</div>
	<script>
		(function() {
			[].slice.call(document.querySelectorAll('select.cs-select'))
					.forEach(function(el) {
						new SelectFx(el, {
							stickyPlaceholder : false
						});
					});
		})();
	</script>
</body>
</html>

