<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<link rel="shortcut icon" href="img//favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/effect2.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />

<title>Конвертер|Вход</title>
</head>
</head>
<body class="content">
	<div id="ip-container" class="ip-container">
		<div class="ip-main">
			<h3>Витебский государственный технологический университет</h3>
			<h2>Конвертер Marc SQL в DSpace</h2>
			<br>
			<hr>
			<br>
			<c:if test="${param['error'] != null}">
				<div id="error" class="error"
					style="color: red; text-align: center;">Неверное имя
					пользователя или пароль.</div>
			</c:if>
			<c:if test="${param['logout'] != null}">
				<div id="error" class="error"
					style="color: red; text-align: center;">Вы вышли.</div>
			</c:if>

			<section class="container">
			<div class="login" style="margin: -53px auto 0 auto;">
				<h1>Авторизация</h1>
				<form method="POST" onsubmit="encode(this);">
					<p>
						<input type="text" id="username" name="username"
							placeholder="Логин">
					</p>
					<p>
						<input type="password" id="password" name="password"
							placeholder="Пароль">
					</p>

					<p class="submit">
						<input type="submit" class="btn" value="Войти">
					</p>
				</form>

			</div>
			</section>

			<div class="ip-main"
				style="text-align: center; font-family: Century Gothic; font-size: 12pt; font-style: italic; ">
				<hr>
				© Библиотека УО "ВГТУ", 2018 | Все права защищены

			</div>
		</div>
	</div>
</body>
</html>