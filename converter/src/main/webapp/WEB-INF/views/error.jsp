<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Конвертер|Ошибка</title>
<script src="${pageContext.request.contextPath}/js/jquery-1.7.2.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.placeholder.min.js"></script>
<script src="${pageContext.request.contextPath}/js/valid.js"></script>
<script src="${pageContext.request.contextPath}/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/js/selectFx.js"></script>
<link rel="shortcut icon" href="img//favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/normalize.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/demo.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/effect2.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/cs-select.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/cs-skin-overlay.css" />
</head>
<body class="content">
	<div id="ip-container" class="ip-container">
		<div class="ip-main">
			<h3>Витебский государственный технологический университет</h3>
			<h2>Конвертер Marc SQL в DSpace</h2>
			<br>
			<a style="color:#000000;  margin-left: 84%; text-decoration: underline" href="logout">Выйти </a>
			<hr>
			<br> <a
				style=" margin-left: 84%; text-decoration: underline"
				href="/converter/">На главную</a>


			<p style="color: red; text-align: center;">${cause}</p>


			<div>
				Показать подробности: <input type="checkbox" value="1"
					onclick="showMe(this)"> <br>

				<div id="div1" style="display: block; display: none; color: red;">
					${exception.cause}
					<c:forEach items="${exception.stackTrace}" var="ste">
						${ste}<br />
					</c:forEach>
				</div>
			</div>
			<div class="ip-main"
				style="text-align: center; font-family: Century Gothic; font-size: 12pt; font-style: italic; ">
				<hr>
				© Библиотека УО "ВГТУ", 2018 | Все права защищены

			</div>
		</div>
	</div>
	<script type="text/javascript">
		function showMe(box) {
			var vis = (box.checked) ? "block" : "none";
			document.getElementById('div1').style.display = vis;
		}
	</script>
</body>
</html>