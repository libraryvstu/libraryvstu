<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Конвертер|Шаг 1</title>
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<link rel="shortcut icon" href="img//favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/effect2.css" />

</head>
<body class="content">
	<div id="ip-container" class="ip-container">
		<div class="ip-main">
			<h3>Витебский государственный технологический университет</h3>
			<h2>Конвертер Marc SQL в DSpace</h2>
			<br>
			 <a style="color:#000000;  margin-left: 84%; text-decoration: underline" href="logout">Выйти </a>
			<hr>
			<br>

			<div class="osnov">
				<div class="forma">
					<form action="search" method="get" id="search-block-form">
						<div class="form-item">
							<input type="text" name="query" value="${param['query']}"
								 placeholder="Найти...">
						</div>
						<div class="form-actions">
							<input type="submit" id="edit-submit" value="Поиск"
								class="form-submit">
						</div>
					</form>
					<div>
						<div class="text">
							<p>
								<i><b>Шаг 1 - Выберете название конференции</b></i>
							</p>
						</div>
					</div>
				</div>
				<br>
				<c:forEach items="${documents}" var="document">
					<div class="info">
						<a class="s5" href='documents/${document.id}'>${document.sources[0].name} - ${document.headers[0].name}</a>
					</div>
				</c:forEach>
				<br>
				<p>Страницы:</p>
				
				<c:forEach begin="0" end="${pages}" var="page">
					<c:choose>
						<c:when test="${not empty param['query']}">
							<a  href='?query=${param["query"]}&pageNumber=${page}'>
								${page}</a>
						</c:when>

						<c:otherwise>
							<a  href='?pageNumber=${page}'> ${page}</a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<div class="ip-main"
					style="text-align: center; font-family: Century Gothic; font-size: 12pt; font-style: italic; ">
					<hr>
					<footer> 
						© Библиотека УО "ВГТУ", 2018 | Все права защищены
					</footer>
				</div>
			</div>
		</div>
	</div>
</body>
</html>