function validate(form) {

	if (form.collectionSel.selectedIndex == 0) {
		onError("error", "Пожалуйста, выберете коллекицию!");
		return false;
	}

	return true;

	if (form.docId == 0) {
		onError("error", "Невозможно импортировать данные, так как статья отсутствует!");
		return false;
	}

	return true;
}

function onError(id, message) {
	document.getElementById(id).innerHTML = message;
}